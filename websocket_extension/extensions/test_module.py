from __future__ import annotations

import asyncio
import traceback
from typing import Type

from aaambos.core.supervision.run_time_manager import ControlMsg
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity

from websocket_extension.extensions.websocket import WebsocketExtension, WebsocketExtensionFeature


class TestModule(Module, ModuleInfo):
    config: TestModuleConfig

    def __init__(self, config: ModuleConfig, com, log, ext: dict, *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.websocket: WebsocketExtension = ext["WebsocketExtension"]

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        pass

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            WebsocketExtensionFeature.name: (WebsocketExtensionFeature, SimpleFeatureNecessity.Required)
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return TestModuleConfig

    async def initialize(self):
        if self.config.connect_serve == "serve":
            await self.websocket.create("my", "localhost", self.handle_receive, 8765)
        else:
            await asyncio.sleep(0.1)
            await self.websocket.connect("my", "ws://localhost:8765", self.handle_receive)
            await asyncio.sleep(0.1)
            await self.websocket.send("my", "0")

    async def handle_receive(self, ref, message):
        try:
            c = int(message)
            self.log.info(c)
            await self.websocket.send(ref, str(c+1))
        except Exception as e:
            traceback.print_exception(e)
            print(message)

    async def step(self):
        pass

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class TestModuleConfig(ModuleConfig):
    module_path: str = "websocket_extension.extensions.test_module"
    module_info: Type[ModuleInfo] = TestModule
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    connect_serve: str = "connect"


def provide_module():
    return TestModule
