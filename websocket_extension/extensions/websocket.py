import asyncio
import traceback
from asyncio.exceptions import CancelledError, TimeoutError
from functools import partial
from typing import Type, Callable, Awaitable, Any
from async_timeout import timeout

import websockets
from semantic_version import Version, SimpleSpec

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature

WEBSOCKET_EXTENSION = "WebsocketExtension"


class WebsocketExtension(Extension):
    """Create and connect to websockets."""

    name = WEBSOCKET_EXTENSION
    active_websockets: dict

    async def before_module_initialize(self, module):
        self.module = module
        self.active_websockets: dict[str, websockets.WebSocketServer] = {}

    async def after_module_initialize(self, module):
        pass

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return WebsocketExtensionSetup

    async def create(self, ref: str, url: str, callback: Callable[[str, Any], Awaitable], port: int, sleep_between_recv: float = 0.05, timeout_loop: float = 0.1):
        await websockets.serve(partial(self.recv_messages, callback=callback, ref=ref, active_websockets=self.active_websockets, sleep_between_recv=sleep_between_recv, timeout_loop=timeout_loop), url, port)

    async def connect(self, ref: str, uri: str, callback: Callable[[str, Any], Awaitable], sleep_between_recv: float = 0.05, timeout_loop: float = 0.1):
        # sleep_between_recv in seconds
        websocket = await websockets.connect(uri)
        asyncio.create_task(self.recv_messages(websocket=websocket, callback=callback, ref=ref, active_websockets=self.active_websockets, sleep_between_recv=sleep_between_recv, timeout_loop=timeout_loop))

    @staticmethod
    async def recv_messages(websocket, callback: Callable[[str, Any], Awaitable], ref: str, active_websockets, sleep_between_recv: float, timeout_loop: float):
        active_websockets[ref] = websocket
        try:
            while True:
                try:
                    async with timeout(timeout_loop):
                        message = await websocket.recv()
                        await callback(ref, message)
                except (CancelledError, TimeoutError):
                    await asyncio.sleep(sleep_between_recv)
        except Exception as e:
            traceback.print_exception(e)
        finally:
            await websocket.close()

    async def send(self, ref: str, message: str):
        if ref in self.active_websockets:
            websocket = self.active_websockets[ref]
            await websocket.send(message)

    def terminate(self, *args, **kwargs):
        loop_ = asyncio.get_event_loop()
        for websocket in self.active_websockets.values():
            loop_.run_until_complete(websocket.close())


class WebsocketExtensionSetup(ExtensionSetup):
    
    def before_module_start(self, module_config) -> Extension:
        return WebsocketExtension()


WebsocketExtensionFeature = ExtensionFeature(
    name=WEBSOCKET_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=WebsocketExtensionSetup,
    as_kwarg=True,
    kwarg_alias="websocket",
)



