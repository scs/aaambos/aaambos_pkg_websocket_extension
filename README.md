# Websocket Extension

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_websocket_extension)

> This is an [AAAMBOS](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/) package. You can find more information about what AAAMBOS is [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/what_is_aaambos) and how to install it [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/).

An extension that can connect and serve websockets
